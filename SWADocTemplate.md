![Logo](media/oy-logo.PNG)

# Student Grading application

Software Architecture documentation for Student Grading application

Version 0.0.1 - 2020-04-22

| Authors          | Contact information   |
| ---------------- | --------------------- |
| Ville Lehtovirta | villel_92@hotmail.com |


**Table of Contents**

[TOC]


---

# Introduction

This document describes and models the software architecture of Student Grading application.

The document is intended to be read by all the stakeholders of the Student Grading development. 

---

# Environment

This chapter discusses the business goals, stakeholders and development environment related issues.

## Business goals

This software is build for easy calculation of course grades for multiple students. The software can read pre-existing data in .txt format and calculate the grades from the data obtained. The output is a single text file containg the grades for students in course. Manual entry of data is also available using the user interface in cases where text data files are not available. The text files read and written follow a specific syntax that could be utilized in other software as well.

Therefore, this software makes the grade calculation process much faster for a course teacher who is responsible for inputting grades. The software provides a toolkit for handling grades using automation. Outputting a file also means other tools can possibly be integrated into using this software. For example, browser scripts could be added to read the output text file and input the data into Weboodi system.

The objective of this software is to reduce manual labor in calculating and inputting course grades.

## Stakeholders

Teachers are the main target user group of this software. They will be responsible for calculating the course grades for students and notifying employees responsible for inputting the grades into Weboodi database.

Other university employees are secondary user group. Users in this group have responsibilities with student grades, for example they can be responsible for inputting the grades into Weboodi database.

Students are a secondary stakeholder for this. Students benefit from this software by having grades available faster, since this software makes the grade calculation and input stage quicker.

## Development environment

This software can run on all computer systems that support C++ and Qt. Desktop and laptop computers are supported. Other mobile devices are not supported.

Qt is an UI tool that can run on multiple operating systems. Latest versions of Windows, Linux and Mac are all supported. Both 32-bit and 64-bit versions of these operating systems are supported as well.

This software is usually ran on a local computer environment, meaning the user launches the application from their computer. As such, internet connection is not a must. Alternatively, the nodes can be ran from different computers as well, for example by running one node per computer then using the IP addresses to configure all the nodes. This requires port forwarding and other setup in the machines.

For further development of this software, C++17 is required as well as Qt Toolkit. 

---

# Architecturally Significant Requirements

This chapter discusses architectural requirements of the application.

## Functional requirements

| Requirement ID | Description                                                                 |
| -------------- | --------------------------------------------------------------------------- |
| UR-001         | Application needs to have support for sending data over the Internet or LAN |
| UR-002         | Application needs to have support for sending data locally                  |
| UR-003         | Application needs to read formatted data text files from a local directory  |
| UR-004         | Application needs to output formatted data text files to a local directory  |
| UR-005         | User needs to be able to input data into the system manually                |
| UR-006         | Application needs to calculate student's grade                              |
| UR-007         | Application needs to have a functional user interface                       |
| UR-008         | Application needs to support Linux, Windows, OSX and some mobile devices    |
| UR-009         | Application needs to be configurable by outgoing and incoming IP addresses  |

## Non-functional requirements

Major non-functional requirements that must be delivered.

| Requirement ID | Description                                                                                      |
| -------------- | ------------------------------------------------------------------------------------------------ |
| NFR-001        | Application needs to have an abstract architectural structure for future changes and reusability |
| NFR-002        | Application needs to have logging for behavior analysis                                          |
| NFR-003        | Application needs to have a reliable data sending system (No data loss)                          |
| NFR-004        | Application needs to be configured using configuration text files                                |
| NFR-005        | Application's sent and received data must be in a reliable syntax                                |
| NFR-006        | Application's sent and received data must be in a reliable syntax                                |

## Constraints

Operating systems supported are constrained by Qt UI plugins. (Refer to https://doc.qt.io/qt-5/supported-platforms.html)
System needs to have enough disk space for the application.
Internet connection is required for sending data between host machines.
The software is licensed in GPL version 2 or later.

---

# Major Architectural Solutions

## Architectural style

Architectural style of the application is Pipes and Filters. The application uses different nodes, that send and receive data in a logical pattern (Pipe) to each other. The first node receives data from a file and processes it, then sends it to the second node for processing (as json), which sends the processed data to the third node and lastly it ends up in the last node, where the data is written to a text file. Each of these nodes have a specific task to do in the pipe. A reason for using Pipes and Filters architecture is that the application can be configured in multiple ways, for example there can be multiple host machines or just one host machine. This way the application can be ran independently without having no dependencies to other systems or organizations (Such as University's IT department).

A node (Also referred to as ProcessorNode) in this application is a filter in the pipe (Pipes & Filters). Node's main task is to receive incoming data, process it accordingly and then send it over to the next node (Or if last node, output a data text file).

As data moves in pipes, the program uses callbacks. In these callbacks, different functions are called depending as the data flows logically in the program. For example, ProcessorNode's handlers are each called in a logical sequence that modifies the data as needed. The event for this sequence to happen is when a new Package object is received in the node.

The architecture in general tries to be as abstract as possible. It also has high encapsulation, meaning most (if not all) class variables are private and if needed, they have setters and getters. Functions are too usually private, unless meant to be used outside the class.

## Design patterns

### Strategy pattern

Strategy design pattern is used upon configuration (runtime). In this design pattern, functions' implementation differs at runtime based on the configuration. E.g. same function can do different things in this case. This is prevalent in node's configuration as different nodes have different tasks. This design pattern serves a purpose in variability, and makes it easy to modify the functionality of a node with variation point being runtime. Example file: https://bitbucket.org/anttijuu/studentnodeelements/src/master/include/StudentNodeElements/GradeCalculator.h

### Prototype pattern

Prototype design pattern is used. This design pattern is used in various data object classes. Using this design pattern, any data item object can cloned easily. This design pattern increases performance, when new object creation is costly. Example file: https://bitbucket.org/anttijuu/studentnodeelements/src/master/StudentDataItem.cpp

### Message queue pattern

Message queue design pattern is used. This is seen in the data flow of the application. Various classes have queues (of data packages) that need to be either read or sent. For example, a node has support for queuing incoming data packages, which is directly related to this design pattern. The queue makes it easier for the application to control the dataflow. In this case, the data is more likely to be valid and error-free. Data package writers and readers are important architectural solutions that are used in the message queue. Example file: https://bitbucket.org/anttijuu/processornode/src/master/ProcessorNode.cpp

### Factory pattern

Factory design pattern is used. In this design pattern, an object can create multiple types of objects without using a traditional object constructor (OOP-principles). For example, for calculating student's grades in different nodes, this pattern is used to determine which grader type needs to be created for the purpose. This makes abstraction of grading easy to use, as the factory can be used instead one of the constructors of its subclasses. This reduces additional logic needed. The design pattern's usage helps the technical side of this application, and does not serve much purpose to the high level user. Example file: https://bitbucket.org/anttijuu/studentnodeelements/src/master/GraderFactory.cpp

### Composite pattern

Composite design pattern is used. This is seen in each node, as this pattern defines the functionality available in the specific node. For example, data packages sent to the node are processed differently in each node, this design pattern ensures that the application is reusable in this regard as these nodes can easily be configured for different functionality. This design pattern makes it easy to add new features to the application or make larger alterations to the functionality. Example files: https://bitbucket.org/anttijuu/processornode/src/master/ProcessorNode.cpp , https://bitbucket.org/anttijuu/studentnodeelements/src/master/StudentInputHandler.cpp

### Template pattern

Template design pattern is used. This design pattern is used in further abstraction of the application which increases reusability. This design pattern mainly reduces boilerplate code for functions that can be same for all subclasses of a type. Example files: https://bitbucket.org/anttijuu/processornode/src/master/ConfigurationDataItem.cpp https://bitbucket.org/anttijuu/studentnodeelements/src/master/StudentDataItem.cpp

## Configuration

The nodes in StudentPassing are configured using configuration files of .txt type. The configuration specifies what type of functions are added to a node. This function defines the functionality of the node, therefore it also defines which type of node it is in the pipe. 

The configuration also specifies the addresses for receiving and sending data. It also specifies where to read and write data (as .txt). For example, the first node (BasicInfoConfig.txt) has a filein parameter, meaning it reads a file from that directory. Another example is that the last node (ProjectInfoConfig.txt) has also a fileout parameter, which is the location for writing the final grade data.

The configuration's variation point is at startup. The project does not need to be compiled again, if wanting to change a configuration. Only the configuration text file needs to be changed in order to modify the setup, then the application can be restarted to read the configuration files and adapt to changes.

Configuration parameters are included in section #.

## Variability techniques

The configuration can be highly variable, as mentioned in the Configuration section. There are also other ways this software has variability. The configuration adds different handlers for each of the nodes, therefore they can function differently. Configuration can also alter the IP addresses of outgoing and incoming data nodes. This variability is defined right before running the application.

The code uses a lot of abstraction by using either abstract classes or interface classes. This way, functions can be reused for similar (but not same) functionality. This variability is defined at compile, but used in runtime. The function variability uses the Strategy design pattern.

The software is also scalable, meaning there can be multiple of same type of nodes. This can increase the performance in cases where the data amount is large. This can also decrease bottlenecks, if one node type is found to have difficulty performance-wise. The software can act somewhat similarly to microservices (Locally). This variability is defined at the same time as configuration (Before running the application).

---

# Architectural views

In this section, the software architecture diagrams are included and the architecture in general is described further.

## Use case view

Main goal of this section is to provide a view to the main functionality and features of the system.

### Actors

| Actor   | Short description                                                                                              |
| ------- | -------------------------------------------------------------------------------------------------------------- |
| Teacher | User responsible for calculating and forwarding the grades to an user who can input them into Weboodi database |

### UC-001 -- Calculating every students' grades for a course by importing data from text files

This is a high level use case, for when all the data required is located in text files. Using the application, the user can easily convert the data obtained into one text file, which contains all grades for students.

### UC-002 -- Calculating every students' grades for a course by entering data manually

This is a high level use case, for when none of the data is located in text files. Using the application, the user inputs all data manually from different node windows. First students are added in BasicInfoConfig, then exercise points are added for students in ExerciseInfoConfig, then exam points are added for students in ExamInfoConfig. Finally, the points from previous nodes are combined with project points in ProjectInfoConfig and then the results are output into a single text file.

### UC-003 -- Get final output text file for grades

In this use case, the teacher gets the outputted text file. This text file can then be inspected for the grades and sent forward, for example by email to other employees.

### UC-004 -- Input student data into the system manually through the User interface

In this user case, the teacher inputs student data manually using the UI elements provided.

### UC-005 -- Copy student data files to a directory for the application to read from

The teacher will have text file containing student data, and then copies those to files to correct directories. These directories can be different for each node.

### UC-006 -- Test node configuration

The teacher will test if the node configuration matches the requirements. 

### UC-007 -- Configure nodes using a configuration file

The teacher will configure nodes attributes using a configuration file.

### UC-008 -- Start node(s)

The teacher will start one or more nodes to be able to process data and transmit it. The technical side of this is explained in a sequence diagram in Other Information subsection.

### UC-009 -- Stop node(s)

The teacher will stop one or more nodes.

### Use case diagram

Use case diagram of UC-001 and UC-002

![Use case diagram](media/UCD.PNG)

### Other information

Sequence diagram of launching StudentPassing (as 2 pictures)

![StudentPassing sequence diagram 1](media/studentpassing_sequence_diagram_1.PNG)

![StudentPassing sequence diagram 2](media/studentpassing_sequence_diagram_2.PNG)

---

## Component view

| Component           | Short description                                                                                                                                                                                                | External? |
| ------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- |
| nlohmann_json       | Used to convert data from/to JSON. For example, Package objects are sent as JSON to other nodes.                                                                                                                 | Y         |
| g3logger            | Used as the logger for the application. Logging is used mainly to analyze application's functionality. Available for all parts of the application (Cross-cutting concern).                                       | Y         |
| Boost               | A library for various different functions used in the application. Though, mainly for Network functionality and UUID generation.                                                                                 | Y         |
| Qt                  | An user interface library to support multiple operating systems (Windows, Linux, Mac).                                                                                                                           | Y         |
| StudentNodeElements | Used for various purposes. For example, ProcessorNode handlers in StudentPassing are defined in this library. Many of the features is included in this library, and it is therefore mandatory for this software. | N         |
| ProcessorNode       | Used for various purposes. It is mandatory for StudentPassing to be able to create nodes at all. Like StudentNodeElements, many features are defined in this library. This component is reusable (Abstract).     | N         |
| StudentPassing      | Implements a ProcessorNode component, which provides functionality for it.                                                                                                                                       | N         |

Component diagram of StudentPassing

![StudentPassing Component Diagram](media/studentpassing_component_diagram.PNG)

---

## Deployment view

The first deployment diagram shows the high level architecture of different nodes. There are  different types of nodes, and their functionality and amount is configurable. Each of these nodes is connected to 1 or 2 other nodes. With the connection, data is transmitted as UDP JSON as Package class objects. Each of the nodes is configured using a configuration file, and they can have a data file from which approriate data is read and used. 

The data files are for example: Student data or exam data. Each of the nodes require different data.

Deployment diagram of the system (without threads)

![Deployment diagram 1](media/node_deployment_diagram_h2.PNG)

The second deployment diagram shows the threads running in each of the nodes. The threads between nodes are similar, though for example the first node has one less thread running than other nodes.

---

## Logical view

### Overview

StudentPassing uses both OHARBase (ProcessorNode package) and StudentNodeElements package.

The two packages OHARBase and StudentNodeElements could be re-utilized in some other project as well, since they are designed to function without StudentPassing as well. StudentPassing on the other hand requires these two packages to function properly.

StudentPassing uses various functions from OHARBase. The most important one is ProcessorNode which is used widely in StudentPassing. A ProcessorNode receives, sends and processes incoming data from other nodes. 

StudentNodeElements provides the Handler classes that the nodes (OHARBase) will use depending on the node's configuration. These handlers have different tasks, but in general, they help the node to process data properly accordingly to the dataflow in the whole application. E.g. first node will receive a different set of handlers compared to other nodes.

Overview of packages on a high level as diagram

![Package level diagram](media/high_level_package_diagram.PNG)

### StudentNodeElements package

This package provides all the Handlers for the application. Handlers are used in ProcessorNodes (refer to next sub-section) as seen in the class diagram. DataHandler class is an abstract class that all Handler classes inherit, therefore adding more Handler is possible using this parent class.

DataItem objects are also abstracted, there can be StudentDataItem objects (and also ConfigurationDataItem objects which are ProcessorNode package's code).

Package objects include a payload and can be consumed by Handlers. In the payload, there is a DataItem object which can be parsed. The Package payload is in JSON format.

There are also Writers and Readers for DataItem objects and its subclass StudentDataItem. These have a simple task of reading and writing these kind of objects to or from std::string type.

Handler classes can be added to ProcessorNode's DataHandler list. This way ProcessorNode's functionality can be abstracted, since the main functionality is provided by the Handler classes in this package. ProcessorNode itself passes data onto the added Handler classes, then they are processed it and passed back (after processing).

In this diagram, factory design pattern is present and is indicated as a Note.

Class diagram of StudentNodeElements

![StudentNodeElements class diagram](media/studentnodeelements_class_diagram.PNG)

#### Handler classes

| Class                   | Short description                                                                                                           |
| ----------------------- | --------------------------------------------------------------------------------------------------------------------------- |
| StudentHandler          | Handles incoming data from previous nodes, or from a data file. Main responsibility is to create student objects from data. |
| StudentNetOutputHandler | Handles data formatting before sending it the next node.                                                                    |
| StudentInputHandler     | Handler incoming data parsing from string format. Inserts it into a StudentDataItem object.                                 |
| StudentWriterHandler    | Writes student data into a file.                                                                                            |
| PlainStudentFileHandler | Determines what if any files need to read. This is determined from the incoming Package's payload.                          |
| GradingHandler          | Calculates a grade for student from a Package.                                                                              |

#### Grader classes

| Class           | Short description                                                                                              |
| --------------- | -------------------------------------------------------------------------------------------------------------- |
| GradeCalculator | Interface class for generic representation of a calculator object.                                             |
| CruelGrader     | Provides grading functionality. Calculates grade (0 to 4 grade) from student's points.                         |
| TheUsualGrader  | Provides default grading functionality (0 to 5 grade). Utilizes a random generator for grades.                 |
| GraderFactory   | Creates an object of GradeCalculator, which is either a CruelGrader or TheUsualGrader. Factory design pattern. |

### ProcessorNode package

This package is responsible for receiving data from earlier nodes, sending data to the next node, and reading the text data files. There can be various different types of nodes, as this package is abstract and reusable. The reusability is made using different Handlers, which are added to a node. These handlers provide the functionality for the node. In addition, different DataItem subclasses can be read in these handlers, this is a way to provide reusability as well. This package is a major component in the whole application, as it receives, process and sends information. A ProcessorNode instance acts as a Filter in the Pipes & Filters architecture style. A pipe is a connection between nodes.

Data between nodes are sent as Package objects using a String object (as json format or plain text). This is then parsed in the node Handlers and instantiated as an object.

Nodes have different functionalities which come from having different Handlers and other variables, which are passed in from StudentPassing (refer to next subsection). Other differences between nodes are:
- The first node does not receive any data from another node. It will read a text data file (or accept manual entry), then process it and send it to the next node. 
- The last node does not send any data to another node. It will receive data from the previous node, read a text data file, then process them and output the final grading to an output text file.

Nodes use callback events in which messages are received and then processed. An event prompts different functions to start. These functions are performed by the Handlers mentioned previously.

In this class diagram, many design patterns are present, which are indicated in Notes.

Class diagram of ProcessorNode

![ProcessorNode class diagram](media/processornode_class_diagram.PNG)

As seen in the object diagram, Package objects payload is converted to a StudentDataItem. Each of the three handlers are consuming the package in a logical sequence. The sequence is described further in another diagram (Data View chapter).

Object diagram of ProcessorNode with ExamInfoConfig -configuration

![Exam Node object diagram](media/examinfoconfig_object_diagram.PNG)

A more basic object diagram of ProcessorNode with ProjectInfoConfig -configuration

![Project node object diagram](media/projectinfoconfig_object_diagram.PNG)

A more basic object diagram of ProcessorNode with BasicInfoConfig -configuration

![Basic node object diagram](media/basicinfoconfig_object_diagram.PNG)

#### Classes

| Class                   | Short description                                                                                            |
| ----------------------- | ------------------------------------------------------------------------------------------------------------ |
| ProcessorNode           | Class that binds the functionality together. Provides reusability through various means (See other classes). |
| ProcessorNodeObserver   | Notifies Observer of events happening in Node. See next subsection (StudentPassing).                         |
| NodeConfiguration       | Class that stores ProcessorNode's configuration as key-value pairs.                                          |
| NetworkReader           | Writes student data into a file.                                                                             |
| NetworkReaderObserver   | Handler incoming data parsing from string format. Inserts it into a StudentDataItem object.                  |
| Networker               | Interface class for networking. Contains functions for receiving and sending data.                           |
| NetworkWriter           | Writes Package objects over network to other Node(s).                                                        |
| DataHandler             | Abstract base class for Handler subclasses.                                                                  |
| PingHandler             | Responsible for constructing Ping (keep-alive) messages to other nodes.                                      |
| ConfigurationHandler    | Handles class for handling configuration type messages.                                                      |
| DataItem                | Abstract base class for DataItem subclass objects.                                                           |
| ConfigurationDataItem   | Class for storing configuration data, such as incoming port and IP.                                          |
| ConfigurationFileReader | Class for reading configuration data files stores in a directory.                                            |
| DataFileReader          | Abstract base class for data reading subclasses.                                                             |
| DataReaderObserver      | Similar to ProcessorNodeObserver. Observes and notifies observers when a data file is read.                  |
| Package                 | Class format to use when sending data over network to other nodes.                                           |

### StudentPassing package

StudentPassing configures the ProcessorNode and assigns it the right Handlers. StudentPassing reads the configuration file and assigns the node class variables, such as input file directory and addresses of other nodes (That are required for the node). The configuration also determines which Handlers are added to instance of ProcessorNode in this package. Bidialog configures the aforementioned Node. ProcessorNodeObserver observes the configuration process in the node, and then notifies Bidialog when configuration is done or changed. 

The Handlers use composite design pattern. The configuration files determine which Handlers are added.

This package also contains the user interface, as such any button presses or other user input go through this package. Packages are sent using the Qt's Signal and Slot features. Therefore, it is also responsible for sending events such as shutdown or start. The user interface (Qt) is also used so the application can be portable to many different systems.

Class diagram of StudentPassing

![StudentPassing class diagram](media/studentpassing_class_diagram.PNG)

---

## Process View

Each node has a main thread, that runs the process. This process acts as the filter in Pipes & Filters. For example, if the application has four different nodes across different host machines, each of these has their own process in the machines. The main application is ran from StudentPassing package, as this contains the UI which the end user interacts with.

As seen in Deployment view section, this software has multiple types of threads running. Since the dataflow in this software may be heavy at times, threads are the most efficient and performant solution. Running concurrent tasks that do not have relation to each other is possible. 

Network receive handler thread is included in all nodes, and it is one I selected to model in an activity diagram. In the thread, there is a variable "running" that is checked every loop, and the thread is exited if it is false. Another attribute to mention is that the thread is checking the message queue once, then the thread sleeps until notification that there is a message in the queue. Then it rechecks running variable and reads packages with available readers. Once it has run all packages, hasIncoming variable is set back to false.

Some threads, such as file reading threads are only active for the duration it takes to read the data file. 

Activity diagram of network receive handler thread

![Receive activity diagram](media/network_receive_handler_thread_activity_diagram.PNG)

NetworkWriter thread is modelled in a state machine diagram. In short, networkWriter thread is only ran when host and port are defined, then it continues to wait for a message to appear in the queue from where it will read, convert and send a Package to the next node. As network receive handler thread, this thread also has "running" variable to decide if further running is needed. This way the thread can continue to wait for more messages, after sending a Package.

State machine diagram of NetworkWriter sending thread

![Send state machine diagram](media/networkwriter_sending_thread_state_machine_diagram.PNG)

ProcessorNode's start() function is modeled in an activity diagram. First, it checks if the Node is already running, meaning it would not need to be started again. If it requires starting, the start sequence begins. Much of the function is surrounded with a try/catch block, which upon failure will log the exception and exit the execution of the software (using stop()). The start sequence consists of starting different class objects, such as networkReader, configReader and their writer equivalents. Also incoming message handler, ioService and commandHandler threads are started. If any of these fail, exception is thrown.

Activity diagram of start function in ProcessorNode

![Start activity diagram](media/start_activity_diagram.PNG)

The next diagram shows a deployment diagram with all the threads running.

Deployment diagram of the system with threads

![Deployment diagram 2](media/node_deployment_diagram_threads.PNG)

### Threads

| Thread name                    | Short description                                                                      |
| ------------------------------ | -------------------------------------------------------------------------------------- |
| StudentPassing main thread     | Main process thread, that is responsible for the whole application                     |
| I/O service thread             | Data input/output thread. Utilizes Boost library.                                      |
| Command handler thread         | Thread responsible for handling user commands and network commands (from other nodes). |
| Network receive handler thread | Handles incoming data from network.                                                    |
| NetworkWriter thread           | Thread for writing data to network (other nodes).                                      |
| PlainStudentFileHandler thread | Thread for reading local data files stored in a directory using StudentFileReader.     |
| StudentHandler thread          | Thread for reading local data files stored in a directory using StudentFileReader.     |

---

## Data View

All nodes have a somewhat similar data flow, a node configured using ExamInfoConfig -configuration is modelled using a Collaboration diagram.

### Files

#### Configuration file

Nodes can be configured using a configuration file (.txt).

The node has a node configuration parameters of: name, config-in, input, output and filein. These variables are described in the table below. The variables are stored using key-value pairs in a text file. The configuration can be read using a DataFileReader object.

Here are the possible parameters for configuration.

| Parameter | Short description                                            |
| --------- | ------------------------------------------------------------ |
| name      | Name of the node                                             |
| config-in | Port where node listens for configuration messages           |
| input     | Port where node listens for message containg Package objects |
| output    | Address of the next node with port                           |
| filein    | Input file where data can be read from                       |
| fileout   | Output file where data is written to                         |

The syntax for the configuration file is as follows:

parameter\tvalue\n

Where \t = tab, \n = line break.
A new parameter can be inserted after the linebreak.

Example file can be found at https://bitbucket.org/anttijuu/studentpassing/src/master/FilesToInstall/StudentPassing/BasicInfoConfig.txt

#### Student Data files

The application can read data files, e.g. files containing student and grading data. These files are read using DataFileReader and then parsed and instantiated as objects. This part can replace manual data entry. These files should follow a key-value pair format similar to configuration data files.

The key value is usually student id, with the value being an array or a string value

Example files can be found at https://bitbucket.org/anttijuu/studentpassing/src/master/FilesToInstall/StudentPassing/basic-student-info.txt https://bitbucket.org/anttijuu/studentpassing/src/master/FilesToInstall/StudentPassing/exercise-info.txt

### Network data

As seen in the diagram, messages are first received by networkReader and then processed and moved onto ProcessorNode where it commands the Handlers to consume the package in the correct order. The messages are first received as bytes, then they are parsed into std::string and then instantiated as a Package object. This whole section is surrounded by a try/catch block. First, StudentInputHandler parses the Package and creates a StudentDataItem of the payload. Then the Package is moved onto StudentHandler where students are read in. Lastly, the Package moves to StudentNetOutputHandler where the processed data is combined with the obtained data (From inputfile) and sent to the next node as a Package message.

StudentDataItem object is instantiated from Package's payload (6,7,8). Payload is first got as string, and is then parsed to JSON. Then the JSON (in correct format) helps create the StudentDataItem object with variables from it.

Data conversion is done on generic level (NetworkWriter, NetworkReader) and application level (StudentInputHandler, StudentNetOutputHandler). Generic level converts Package types, while application level converts the payload of it.

Collaboration diagram of ProcessorNode with ExamInfoConfig -configuration

![Image](media/examinfoconfig_collaboration_diagram.PNG)

All messages are sent as UDP as String JSON format. The payload format can be a JSON as well, thought it can be a text string as well. The payload in messages is serialized using nlohmann_json. This converts the JSON payload to string (from_json()), from which it can be processed further. Marshalling the data from string to JSON is also done using nlohmann_json, function for this is to_json().

JSON format uses parameters (as key-value pair in string type). JSON is a popular format in network messages, as such it is a preferred format here as well.

Messages are of format:

```JSON
{ 
   "package" : "123e4567-e89b-12d3-a456-426655440000",
   "type" : "command" | "data" | "configuration",
   "payload" : "command value" | "payload in json"
}
```

#### Example messages:

Package messages are used to transmit processed data to next node.

Package message:

```JSON
{ 
   "package" : "123e4567-e89b-12d3-a456-426655440000",
   "type" : "data",
   "payload" :
   {
      "id" : "12345",
      "name" : "Test Student",
      "studyprogram" : "TOL",
      "exampoints" : 15,
      "exercisepoints" : [8,10,3,1,8,0,11],
      "courseprojectpoints" : 16,
      "grade" : 3
   }
}
```

Configuration messages are used to configure the nodes.

Configuration message:

```JSON
{
   "package" : "123e4567-e89b-12d3-a456-426655440000",
   "type" : "configuration",
   "payload" : 
   {
      "operation" : "set",
      "configitems" : [
         {"name" : "node name"},
         {"config-item" : "item-value"},
         {"config-item2" : "item-value"}
      ]
   }
}
```

Command messages are used to transmit commands, such as shutdown or start.

Command message:

```JSON
{
   "package" : "123e4567-e89b-12d3-a456-426655440000",
   "type" : "command",
   "payload" : "shutdown"
}
```

# Analysis (TODO: In phase 2)

After the architecture **design** is ready or almost ready, it **should be reviewed** to confirm that the solutions in the design do implement the essential functional and especially non-functional requirements. This can be done using architecture evaluation methods, such as ATAM.

This section of the document should summarize the results of the evaluation:

* does the architecture have **risks**; solutions which could lead to unsatisfactory results considering one or more non-functional requirement?
* are there any **sensitivity points**; solutions that are sensitive towards some non-functional requirement in a sense that the solution is removed or changed, it could lead to the requirement not being met?
* are there any **tradeoff points**; solutions that satisfy some requirement but negatively effects some other requirement?
* what are the **non-risks**, solutions which do help meeting requirement(s) and have no other consequences?

# References (TODO: In phase 2)

Other documents, artifacts, files, models that were used in the design and modelling the arcitecture of the software.

# Appendices (TODO: In phase 2)

Possible appendices that were not included above but are relevant for the design process and/or the end result.
